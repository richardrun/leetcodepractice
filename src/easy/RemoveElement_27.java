package easy;

import java.util.Arrays;

public class RemoveElement_27 {

    public void test(){
        int[] temp = new int[]{0,1,2,2,3,0,4,2};
        removeElement(temp,2);
    }

    public int removeElement(int[] nums, int val) {
        int count=0;
        if(nums==null || nums.length==0) return count;

        int left,right;
        left = right = count;

        while(right<nums.length){
            //System.out.println(Arrays.toString(nums));
            if(nums[right] != val) {
                nums[left] = nums[right];
                left++;
                count++;
            }
            //System.out.println(Arrays.toString(nums));
            right++;
        }
        return count;
    }
}
