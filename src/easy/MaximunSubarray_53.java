package easy;

public class MaximunSubarray_53 {
    public int maxSubArray(int[] nums) {
        if(nums==null || nums.length==0) return 0;

        int max = Integer.MIN_VALUE;
        int curMax = 0;

        for(int i: nums){
            curMax = Math.max(i, curMax+i);
            max = Math.max(curMax,max);
        }

        return max==Integer.MIN_VALUE? 0:max;
    }
}
