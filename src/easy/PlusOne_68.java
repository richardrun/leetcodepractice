package easy;

public class PlusOne_68 {
    public int[] plusOne(int[] digits) {
        if(digits==null || digits.length==0) return digits;



        digits[digits.length-1]++;

        for(int i=digits.length-1; i>0; i--){
            if(digits[i] > 9){
                digits[i] = 0;
                digits[i-1]++;
            }else break;
        }

        if(digits[0] >9){
            digits[0] = 0;
            int[] ret = new int[digits.length+1];
            ret[0] = 1;
            System.arraycopy(digits,0,ret,1,digits.length-1);
            return ret;
        }
        return digits;

    }
}
