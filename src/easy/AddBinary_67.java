package easy;

import java.util.Arrays;

public class AddBinary_67 {
    public String addBinary(String a, String b) {
        StringBuilder s1 = new StringBuilder(a);
        StringBuilder s2 = new StringBuilder((b));
        int s1Length = s1.length()-1;
        int s2Length = s2.length()-1;
        char[] ret = new char[Math.max(s1Length,s2Length)+1];
        System.out.println("char lenght" + ret.length);
        //int i = length;
        int sum = 0;
        int arrIndex = ret.length-1;
        while (s1Length>=0 || s2Length>=0){
            System.out.println("s1 length: " + s1Length);
            System.out.println("s2 length: " + s2Length);

            if(s1Length>=0 && s1.charAt(s1Length)=='1') {
                sum++;
            }
            if(s2Length>=0 && s2.charAt(s2Length) == '1') {
                sum++;
            }
            s1Length--;
            s2Length--;

            System.out.println("sum: " + sum);

            ret[arrIndex] = (char) ((sum%2) + '0');
            arrIndex--;
            if(sum>=2) {
                sum = 1;
            }else sum = 0;

            System.out.println("string: " + Arrays.toString(ret));

        }

        System.out.println("string: " + Arrays.toString(ret));
        if(sum==1) return ("1" + new String(ret));
        else return new String(ret);
    }
}
