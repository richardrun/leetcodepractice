package easy;

import java.util.HashMap;
import java.util.Map;

public class TwoSum_1 {
    public int[] twoSum(int[] nums, int target){
        Map<Integer, Integer> hashmap = new HashMap<>();
        int[] ret = new int[2];

        for(int i=0 ;i< nums.length; i++){
            if(hashmap.containsKey(target - nums[i])){
                ret[0] = hashmap.get(target - nums[i]);
                ret[1] = i;
                return ret;
            }
            hashmap.put(nums[i], i);
        }
        return  ret;
    }
}
