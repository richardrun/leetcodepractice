package easy;

public class LengthOfLastWord_58 {
    public int lengthOfLastWord(String s) {
        if(s==null || s.length()==0) return 0;

        char[] arr = s.toCharArray();
        boolean isWord = false;
        int count = 0;

        for(int i=arr.length-1; i>=0; i--){
            if(Character.isLetter(arr[i])){
                isWord  = true;
                count++;
            }else if(isWord) break;
        }
        return count;
    }
}
