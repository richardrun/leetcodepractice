package easy;

import java.util.Arrays;

public class RemoveDuplicatesFromSortedArray_26 {
    public void test(){
        int[] temp = new int[]{1,1,2};
        removeDuplicates(temp);
    }

    public int removeDuplicates(int[] nums) {
        if(nums==null) return 0;
        if(nums.length<2) return 1;

        int count = 1;
        int left =0;
        int right = 1;

        while(right < nums.length){
            //System.out.println(right);

            //System.out.println(Arrays.toString(nums));
            if(nums[left] == nums[right]){
                //nums[left+1] = nums[right];
            }else {
                nums[left+1] = nums[right];
                count++;
                left++;
            }
            //System.out.println(Arrays.toString(nums));

            right++;
        }
        return count;
    }
}
