package easy;

public class ReverseInteger_7 {
    public int solution(int x){

        boolean positive = x>=0;
        x = positive? x: x*-1;
        int ret = 0;

        while(x>0){
            int oldValue = ret;

            ret = ret*10;
            if(ret/10 != oldValue ) return 0;
            ret += x%10;
            x/=10;

        }



        return positive?ret:ret*-1;
    }
}
