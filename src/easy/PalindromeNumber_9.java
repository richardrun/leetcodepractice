package easy;

public class PalindromeNumber_9 {
    public boolean isPalindrome(int x){
        if(x<0) return false;

        int rev = 0;
        int originalX = x;
        while (x>0){
            int oldRev = rev;
            rev *= 10;
            if(rev/10 != oldRev) return false;
            rev += x%10;
            x/=10;
        }

        return originalX == rev;
    }
}
