package hard;

import java.util.*;

public class LRUCache {

    Deque<Integer> dq = new ArrayDeque<>();
    Map<Integer, Integer> map = new HashMap<>();
    int maxSize;
    public LRUCache(int capacity) {
        maxSize = capacity;
    }

    public int get(int key) {

        int value = map.getOrDefault(key,-1);

        System.out.println("value: " + value);
        if(value != -1){
            dq.remove(key);
            dq.addLast(key);
        }
        System.out.println("get " + key);
        System.out.println(map.toString());
        System.out.println(dq.toString());
        return value;

    }

    public void put(int key, int value) {

        if(map.get(key) != null && dq.size()>0) {
            dq.remove(key);
            dq.addLast(key);
        }else if(dq.size()>=maxSize){
            int temp = dq.removeFirst();
            dq.addLast(key);
            map.remove(temp);
        }else{
            dq.addLast(key);
        }
        map.put(key,value);
        System.out.println("put " + key +" " + value);
        System.out.println(map.toString());
        System.out.println(dq.toString());


    }
}
