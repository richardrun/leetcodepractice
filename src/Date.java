public class Date {

//make the days of month for all to use
    private int[] daysOfMonth = new int[]{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int daysOfYear = 365;


    private int year;
    private int month;
    private int day;

//default constructor
    public Date() {
        this.year = 1970;
        this.month = 1;
        this.day = 1;
    }
//constructor
    public Date(int year, int month, int day){


        this.year = year;
        this.month = month;
        this.day = day;
        if (!(isMonthValid(month) && isDayValid(day))) throw new IllegalArgumentException();

    }
//check if the day is valid
    public boolean isDayValid(int day){
        return day >0 && day <= daysOfMonth[this.month-1];
    }
//check if the month is valid
    public boolean isMonthValid(int month){
        return month <13 && month >= 1;
    }

    public void addDays(int days){

        if(isLeapYear()) daysOfMonth[1] = 29;

        int totalDaysOfYear = toTotalsDays();

        totalDaysOfYear += days;

        toDate(totalDaysOfYear);


        daysOfMonth[1] = 28;
    }

    public void addWeeks(int weeks){
        int daysOfWeeks = weeks *7;
        addDays(daysOfWeeks);
    }

    public int getYear() {
        return year;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int toTotalsDays(){
        int totalDays = 0;
        for(int i=0; i< month-1; i++){
            totalDays += daysOfMonth[i];
        }
        totalDays += day;

        return totalDays;
    }

    public String longDate() {
        String[] monthArr = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        String output = monthArr[month - 1] + " " + day + ", " + year;
        return output;
    }

    public static int daysTo(Date one, Date two){
        int diffYear = one.getYear() - two.getYear();
        int diffMonth = one.getMonth() - two.getMonth();
        int diffDay = one.getDay() - two.getDay();

        int totalDiff = diffYear * 365 + diffMonth * 31 + diffDay;

        //return negative if diffYear is negative, else return positive
        return diffYear < 0? totalDiff*-1 : totalDiff;
    }

    public int daysTo(Date two){
        int diffYear = this.getYear() - two.getYear();
        int diffMonth = this.getMonth() - two.getMonth();
        int diffDay = this.getDay() - two.getDay();

        int totalDiff = diffYear * 365 + diffMonth * 31 + diffDay;

        //return negative if diffYear is negative, else return positive
        return diffYear < 0? totalDiff : totalDiff * -1;
    }

    public void toDate(int totalDays){
        if(isLeapYear()) {
            daysOfYear = 366;
            daysOfMonth[1] = 29;
        }

        int years = totalDays / daysOfYear;
        totalDays %= daysOfYear;


        int[] monthAndDay = calMonthsAndDays(totalDays);

        this.year += years;
        this.month = monthAndDay[0];
        this.day = monthAndDay[1];

        daysOfMonth[1] = 28;
        daysOfYear = 365;
    }

    public int[] calMonthsAndDays(int totalDays){
        int[] monthAndDay = new int[2];
        int months = 1;

        for(int i=0; i<daysOfMonth.length; i++){
            if(totalDays > daysOfMonth[i]) {
                totalDays -= daysOfMonth[i];
                months++;
            }else{
                monthAndDay[0] = months;
                monthAndDay[1] = totalDays;
                break;
            }
        }
        return monthAndDay;
    }


    public boolean isLeapYear() {
        if (year % 4 != 0) {
            return false;
        } else if (year % 400 == 0) {
            return true;
        } else if (year % 100 == 0) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public String toString() {
        return this.year + "/" + this.month + "/" + this.day;
    }
}
